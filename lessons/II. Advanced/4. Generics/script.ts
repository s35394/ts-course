// Generics

// Interface

interface City<T> {
    name: string,
    pop: number,
    additionalData: T
}

const London : City<object> = {
    name: "London",
    pop: 10,
    additionalData: {area: 1572}
}

const Paris : City<object[]> = {
    name: "Paris",
    pop: 5,
    additionalData: [{
        underground: true,
        lines: 57,
    }, {
        restaurant: true,
        number: 558
    }]
}

// Generics functions

const addRepairDate = <T extends object> (obj: T) => {
    const lastRepair = new Date()
    return {...obj, lastRepair}
}

const auto1 = addRepairDate({
    model: "A1",
    km: 700000,
    price: 210000
})

console.log(auto1.model)
console.log(auto1.km)
console.log(auto1.price)

const auto2 = addRepairDate({
    model: "A1",
    km: 700000,
    price: 210000,
    color: "red"
})