"use strict";
// Generics
const London = {
    name: "London",
    pop: 10,
    additionalData: { area: 1572 }
};
const Paris = {
    name: "Paris",
    pop: 5,
    additionalData: [{
            underground: true,
            lines: 57,
        }, {
            restaurant: true,
            number: 558
        }]
};
// Generics functions
const addRepairDate = (obj) => {
    const lastRepair = new Date();
    return Object.assign(Object.assign({}, obj), { lastRepair });
};
const auto1 = addRepairDate({
    model: "A1",
    km: 700000,
    price: 210000
});
console.log(auto1.model);
console.log(auto1.km);
console.log(auto1.price);
const auto2 = addRepairDate({
    model: "A1",
    km: 700000,
    price: 210000,
    color: "red"
});
