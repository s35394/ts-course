"use strict";
// @types/jquery
// Operator !
const container = document.querySelector(".container");
console.log(container.children);
const user1 = {
    title: "DevOps",
    description: "Docker et Kubernetes",
    salary: 30000
};
console.log(user1 === null || user1 === void 0 ? void 0 : user1.description);
// Optional Parameter
function message(msg) {
    if (msg) {
        console.log(msg);
    }
    else {
        console.log("No message provided");
    }
}
message("hello world");
const House1 = {
    room: 4,
    price: 25000
};
// Operator ??
const data = 0;
const display = data !== null && data !== void 0 ? data : "Hello World";
console.log(display);
//  Never
function alertUser(msg) {
    throw msg;
}
alertUser("Alerte, comportement dangereux");
