// @types/jquery

// Operator !
const container = document.querySelector(".container")!
console.log(container.children)

// Operator ?
type Job = {
    title: string, 
    description?: string,
    salary: number
}

const user1 : Job = {
    title: "DevOps",
    description: "Docker et Kubernetes",
    salary: 30000
}

console.log(user1?.description)


// Optional Parameter
function message(msg? : string) {
    if(msg) {
        console.log(msg)
    }
    else {
        console.log("No message provided")
    }
}

message("hello world")

interface House {
    room: number,
    price: number,
    garage?: number
}

const House1: House = {
    room: 4,
    price: 25000
}


// Operator ??
const data = 0
const display = data ?? "Hello World"

console.log(display)

//  Never

function alertUser(msg: string): never {
    throw msg
}

alertUser("Alerte, comportement dangereux")