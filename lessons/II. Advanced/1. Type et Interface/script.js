"use strict";
// Intersection
const shark1 = {
    fin: 3,
    element: "water",
    gills: true,
    weight: 500,
    length: 500
};
let object;
const RedRose = {
    pollen: true,
    type: "vegetal",
    color: "blue",
    thorn: true
};
const automaticResponse = (country) => {
    if (country.lang === "JA") {
        console.log("Welcome, Japan");
    }
    else if (country.lang === "IT") {
        console.log("Welcome, Italy");
    }
};
const Japanese = {
    lang: "JA",
    food: ["Ramen", "Sushis"]
};
automaticResponse(Japanese);
const spainTrip = {
    john: { id: 1 },
    toto: { id: 2 },
    jules: { id: 3 }
};
console.log(spainTrip);
