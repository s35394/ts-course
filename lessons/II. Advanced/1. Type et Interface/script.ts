// Intersection

type Fish = {
    fin: number
    element: "water"
    gills: true
}

type Shark = {
    weight: number
    length: number
}

type Hammerhead = Fish & Shark

const shark1: Hammerhead = {
    fin: 3,
    element: "water",
    gills: true,
    weight: 500,
    length: 500
}

let object: {
    prop1 : "a"
} & {
    prop2 : "b"
}

interface Flower {
    pollen: true
    type: "vegetal"
}

interface Rose extends Flower {
    color: string
    thorn: boolean
}

const RedRose: Rose = {
    pollen: true,
    type: "vegetal",
    color: "blue",
    thorn: true
}

// Discriminated Union

type Japan = {
    lang: "JA"
    food: string[]
}

type Italy = {
    lang: "IT"
    food: string[]
}

type Country = Japan | Italy

const automaticResponse = (country: Country) => {
    if(country.lang === "JA") {
        console.log("Welcome, Japan")
    }
    else if (country.lang === "IT") {
        console.log("Welcome, Italy")
    }
}

const Japanese: Country = {
    lang: "JA",
    food: ["Ramen", "Sushis"]
}

automaticResponse(Japanese)


// Unknown props length

interface Group {
    [name: string] : object,
}

const spainTrip: Group = {
    john: {id:1},
    toto: {id:2},
    jules: {id:3}
}

console.log(spainTrip)