"use strict";
//Unions
let code;
code = 5;
let array;
array = [true, false, 999];
const unionFoo = (param) => {
    console.log(param);
};
unionFoo('Test');
const unionBaz = (param) => {
    console.log(param);
};
unionBaz(true);
const button = {
    x: 99,
    y: 50,
    id: 999,
    visible: true
};
