//Unions
let code : string | number | Function
code = 5

let array: (boolean | number)[]
array = [true, false, 999]

const unionFoo = (param: number | string) => {
    console.log(param)
}

unionFoo('Test')

//Types Aliases
type mixedNumStr = number | string
type booleanOrObject = boolean | object

const unionBaz = (param: mixedNumStr | booleanOrObject) => {
    console.log(param)
}
unionBaz(true)

type element = {
    x: number;
    y: number;
    id: number | string;
    visible: boolean;
}

const button : element = {
    x: 99,
    y: 50,
    id: 999,
    visible: true
}