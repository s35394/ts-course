// Tuple
let tuple : [boolean, number]
tuple = [true, 20]
console.log(tuple)

// Enum

enum Roles {
    JAVASCRIPT = 1,
    CSS,
    REACT,
    PHP,
    MYSQL,
}

console.log(Roles)
console.log(Roles.JAVASCRIPT)