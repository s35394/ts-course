"use strict";
// Tuple
let tuple;
tuple = [true, 20];
console.log(tuple);
// Enum
var Roles;
(function (Roles) {
    Roles[Roles["JAVASCRIPT"] = 1] = "JAVASCRIPT";
    Roles[Roles["CSS"] = 2] = "CSS";
    Roles[Roles["REACT"] = 3] = "REACT";
    Roles[Roles["PHP"] = 4] = "PHP";
    Roles[Roles["MYSQL"] = 5] = "MYSQL";
})(Roles || (Roles = {}));
console.log(Roles);
console.log(Roles.JAVASCRIPT);
