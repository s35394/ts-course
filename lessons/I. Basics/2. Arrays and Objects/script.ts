// arrays
const fruits = [
    'fraise',
    'pomme'
]
fruits.push('cerise')
console.log(fruits)

const mixedArray = [
    1,
    'txt',
    [1,2,3]
]
mixedArray.push(
    [
        2,
        3,
        4
    ]
)
console.log(mixedArray)

let nums: number[]
nums = [1,2,3]

let nums2 : number[] = []
nums2.push(2)

// objects
const car = {
    name: 'Audi', // car.name =4 !erreur
    model: 'A1',
    km: 70000
}

let profile: {
    name: string,
    age: number
} = {
    name: "John",
    age : 35
}