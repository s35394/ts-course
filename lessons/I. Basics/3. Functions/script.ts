function multiply(num1: number, num2: number, action?: string){
    if (action) console.log(action)
    return num1 * num2;
}
console.log(multiply(6, 10))

let foo: Function
foo = () => {}

// Function signatures
let baz: (a: number, b: number) => number
baz = (a,b) => a+b

// Function callbacks
function greetings(fn: (a: string) => void) {
    fn("hello world")
}

function printConsole(msg: string) {
    console.log(msg)
}

greetings(printConsole)