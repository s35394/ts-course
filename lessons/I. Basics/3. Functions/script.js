"use strict";
function multiply(num1, num2, action) {
    if (action)
        console.log(action);
    return num1 * num2;
}
console.log(multiply(6, 10));
let foo;
foo = () => { };
// Function signatures
let baz;
baz = (a, b) => a + b;
// Function callbacks
function greetings(fn) {
    fn("hello world");
}
function printConsole(msg) {
    console.log(msg);
}
greetings(printConsole);
