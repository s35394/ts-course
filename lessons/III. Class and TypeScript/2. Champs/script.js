"use strict";
class Books {
    constructor(title, price, dateOfParution, author, theme) {
        this.title = title;
        this.price = price;
        this.dateOfParution = dateOfParution;
        this.author = author;
        this.theme = theme;
        this.bookID = 88;
        this.libraryName = "The Book Shelf";
    }
    promo() {
        console.log("ID", this.bookID);
        console.log("NAME", this.libraryName);
        return this.price * 0.5;
    }
}
const books1 = new Books("The Great Gatsby", 20, "11/04/1998", "Tom Joe");
console.log(books1);
console.log(books1.promo());
