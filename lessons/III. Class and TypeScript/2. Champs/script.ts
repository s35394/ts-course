class Books {
    private bookID = 88
    readonly libraryName = "The Book Shelf"

    constructor(
        public title: string,
        public price: number,
        public dateOfParution: string,
        public author: string,
        public theme?: string[]
    ) {}

    promo() {
        console.log("ID", this.bookID)
        console.log("NAME", this.libraryName)
        return this.price * 0.5
    }
}

const books1 = new Books (
    "The Great Gatsby",
    20,
    "11/04/1998",
    "Tom Joe"
)

console.log(books1)
console.log(books1.promo())
