interface country {
    name: string,
    population: number,
    lang: string[]
}

class Norway implements country {
    constructor(
        public name: string,
        public population: number,
        public lang: string[],
        public capital: string
    ) {}
}

const NorwayData = new Norway('Norway', 9, ["Norwegian"], "Oslo")
console.log(NorwayData)

class France implements country {
    constructor(
        public name: string,
        public population: number,
        public lang: string[]
    ) {}
}

const FranceData = new France('France', 70, [
    "Français",
    "Breton",
    "Occitan",
    "Basque",
    "Alsacien",
    "Corse"
])
console.log(FranceData)

class Occitanie extends France {}
const OccitanieData = new Occitanie (
    "Occitanie",
    35,
    ["Français"]
)