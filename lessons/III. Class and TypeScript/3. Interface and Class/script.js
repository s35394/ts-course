"use strict";
class Norway {
    constructor(name, population, lang, capital) {
        this.name = name;
        this.population = population;
        this.lang = lang;
        this.capital = capital;
    }
}
const NorwayData = new Norway('Norway', 9, ["Norwegian"], "Oslo");
console.log(NorwayData);
class France {
    constructor(name, population, lang) {
        this.name = name;
        this.population = population;
        this.lang = lang;
    }
}
const FranceData = new France('France', 70, [
    "Français",
    "Breton",
    "Occitan",
    "Basque",
    "Alsacien",
    "Corse"
]);
console.log(FranceData);
class Occitanie extends France {
}
const OccitanieData = new Occitanie("Occitanie", 35, ["Français"]);
